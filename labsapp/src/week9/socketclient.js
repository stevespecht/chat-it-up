import React from "react";
import io from "socket.io-client";

class SocketClient extends React.PureComponent {
  state = {
    msg: ""
  };
  componentDidMount = async () => {
    // connect to server
    const socket = io.connect("localhost:5000", { forceNew: true });
    // send join message to server, pass a payload to it
    //socket.emit("join", { name: "React Client", room: "room1" }, err => {});
    //socket.on("welcome", greetingsMsg => this.setState({ msg: greetingsMsg }));

    // send join message to server, pass a payload to it
    socket.emit(
      "join",
      { name: this.props.name, room: this.props.room },
      err => {}
    );
    // handle welcome message from server
    socket.on("welcome", greetingsMsg => {
      if (this.state.msg === "") {
        this.setState({ msg: greetingsMsg });
      }
    });
  };

  render() {
    return <div>{this.state.msg}</div>;
  }
}
export default SocketClient;

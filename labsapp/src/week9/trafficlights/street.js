import React from "react";
import "../../App.css";
import TrafficLight from "./trafficlights";

const Street = () => (
  <div className="flex-container" float="right">
    <TrafficLight street="Steven" />
    <TrafficLight street="Specht" />
    <TrafficLight street="Info3069" />
    <TrafficLight street="Heroku" />
  </div>
);
export default Street;

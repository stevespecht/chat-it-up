import React from "react";
import io from "socket.io-client";

class TrafficLight extends React.PureComponent {
  state = {
    currentcolor: "",
    red: 0,
    green: 0,
    yellow: 0
  };

  componentDidMount = async () => {
    // connect to server
    //const socket = io.connect("localhost:5000", { forceNew: true });

    //connect to server on Heroku cloud
    const socket = io.connect();

    // send join message to server, pass a payload to it
    socket.emit(
      "join",
      { name: this.props.street, room: this.props.room },
      err => {}
    );
    // handle welcome message from server
    socket.on("turnLampOn", newLamp => {
      this.setState(
        {
          red: newLamp.red,
          green: newLamp.green,
          yellow: newLamp.yellow
        },
        function() {
          this.turnLampOnHandler();
        }
      );
    });
  };

  turnLampOnHandler = async () => {
    while (true) {
      await this.waitSomeSeconds("red", this.state.red);
      await this.waitSomeSeconds("green", this.state.green);
      await this.waitSomeSeconds("yellow", this.state.yellow);
    }
  };

  waitSomeSeconds = async (color, wait) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.setState({ currentcolor: color });
        resolve();
      }, wait);
    });
  };

  getColor = color => (this.state.currentcolor === color ? color : "white");

  render() {
    return (
      <div className="light">
        <div
          className="lamp"
          style={{ backgroundColor: this.getColor("red"), margin: ".5rem" }}
        />
        <div
          className="lamp"
          style={{ backgroundColor: this.getColor("yellow"), margin: ".5rem" }}
        />
        <div
          className="lamp"
          style={{ backgroundColor: this.getColor("green"), margin: ".5rem" }}
        />
        <div style={{ textAlign: "center", fontName: "Helvetica" }}>
          {this.props.street}
        </div>
      </div>
    );
  }
}
export default TrafficLight;

import React from "react";
import "../App.css";
class PureClassyComponent extends React.PureComponent {
  state = {
    message: "",
    word: ""
  };

  handleChange(msg) {
    this.setState({ word: msg });
  }
  addWord() {
    this.setState({
      message: this.state.message + " " + this.state.word,
      word: ""
    });
  }

  clear() {
    this.setState({ message: "", word: "" });
  }
  render() {
    return (
      <h4>
        The message is: {this.state.message}
        <p />
        <input
          type="text"
          value={this.state.word}
          onChange={event => this.handleChange(event.target.value)}
        />
        <input type="submit" value="Add word" onClick={() => this.addWord()} />
        <p />
        <input type="submit" value="clear msg" onClick={() => this.clear()} />
      </h4>
    );
  }
}
export default PureClassyComponent;

import React from "react";
import ReactDOM from "react-dom";
import PureClassyComponent from "./week6/purecomponent";
ReactDOM.render(<PureClassyComponent />, document.getElementById("root"));

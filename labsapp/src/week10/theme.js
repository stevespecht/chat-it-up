import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "#fafafa" },
    primary: {
      light: "rgba(255, 192, 89, 1)",
      main: "rgba(246, 177, 64, 1)",
      dark: "rgba(255, 159, 0, 1)",
      contrastText: "#fff"
    },
    secondary: {
      light: "rgba(73, 242, 255, 1)",
      main: "rgba(0, 228, 245, 1)",
      dark: "rgba(0, 168, 180, 1)",
      contrastText: "#fff"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});

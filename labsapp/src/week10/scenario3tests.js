import React from "react";
import io from "socket.io-client";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Button, TextField, Typography } from "@material-ui/core";
import theme from "./theme";

class Scenario3Tests extends React.PureComponent {
  state = {
    socket: null,
    messages: [],
    chatName: "",
    roomName: "main",
    msg: "",
    isTyping: false,
    hideJoinObjects: false
  };

  componentDidMount = () => {
    // connect to server
    const socket = io.connect("localhost:5000", { forceNew: true });
    this.setState({ socket: socket });
    socket.on("welcome", this.onWelcome);
    socket.on("nameexists", this.addMessage);
    socket.on("someonejoined", this.addMessage);
    socket.on("someoneleft", this.addMessage);
    socket.on("someoneistyping", this.addMessage);
  };

  // handler for Join button click
  handleJoin = () => {
    this.state.socket.emit("join", {
      chatName: this.state.chatName,
      roomName: this.state.roomName
    });
  };
  // handler for name TextField entry
  onNameChange = e => {
    this.setState({ chatName: e.target.value });
  };

  // it is best practice to give a function to setState that uses
  // the previous state and returns the state update
  addMessage = dataFromServer => {
    this.setState(previousState => {
      let messages = [...previousState.messages];
      messages.push(dataFromServer);
      return { messages };
    });
  };

  onWelcome = dataFromServer => {
    this.addMessage(dataFromServer);
    this.setState({ hideJoinObjects: true });
  };

  // handler for message TextField entry
  onMessageChange = e => {
    this.setState({ msg: e.target.value });
    if (this.state.isTyping === false) {
      this.state.socket.emit("typing", {
        chatName: this.state.chatName,
        roomName: this.state.roomName
      });
      this.setState({ isTyping: true });
    }
  };

  render() {
    const { messages, chatName, hideJoinObjects } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <h2 style={{ textAlign: "center" }}>Lab 9 - Scenario 1,2,3 Tests</h2>

        {!hideJoinObjects && (
          <React.Fragment>
            <TextField
              onChange={this.onNameChange}
              placeholder="Enter unique name"
              autoFocus={true}
              required
              value={chatName}
            />
            <br />
            <Button
              onClick={this.handleJoin}
              color="primary"
              variant="contained"
              style={{ marginTop: "1em" }}
            >
              Join
            </Button>
          </React.Fragment>
        )}

        {hideJoinObjects && (
          <TextField
            onChange={this.onMessageChange}
            placeholder="type something here"
            autoFocus={true}
            required
          />
        )}

        {messages.map((message, index) => (
          <Typography key={index} style={{ marginTop: "3rem" }}>
            {message.text}
          </Typography>
        ))}
      </MuiThemeProvider>
    );
  }
}
export default Scenario3Tests;

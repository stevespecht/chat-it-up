import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "#fafafa" },
    primary: {
      light: "rgba(251, 201, 120, 1)",
      main: "rgba(245, 166, 35, 1)",
      dark: "rgba(189, 120, 0, 1)",
      contrastText: "#fff"
    },
    secondary: {
      light: "rgba(217, 88, 243, 1)",
      main: "rgba(189, 16, 224, 1)",
      dark: "rgba(126, 3, 151, 1)",
      contrastText: "#fff"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});

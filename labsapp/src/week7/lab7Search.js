import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Snackbar,
  Typography
} from "@material-ui/core";
import theme from "./theme";
import Autosuggest from "react-autosuggest";
import "../App.css";

let url = "";
// non optional method - not used
const getSuggestionValue = suggestion => suggestion.name;
// renders filtered results
const renderFilteredList = selection => (
  <Typography variant="h6" color="inherit">
    {selection.name}
  </Typography>
);

class MaterialUIExLab7Search extends React.PureComponent {
  state = {
    filteredUsers: [], // what we're searching through
    value: "", // what the user enters
    userInfo: "", // what we'll display when a selection is made
    users: [],
    gotData: false,
    selectedMsg: "",
    snackbarMsg: "",
    userJson: []
  };

  // filtering code
  getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    return inputLength === 0
      ? ""
      : this.state.users.filter(
          user => user.name.toLowerCase().slice(0, inputLength) === inputValue
        );
  };

  componentDidMount() {
    url = this.props.url;
    this.getUsers();
  }
  // this stores the filtered list into state
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      filteredUsers: this.getSuggestions(value)
    });
  };
  // user has made a choice
  onSuggestionSelected = (event, { suggestion }) => {
    this.setState({
      userInfo: `User ${suggestion.name} is ${
        suggestion.age
      } years old and can be reached by email at ${suggestion.email}`
    });
  };
  // user has typed something in the input
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
      userInfo: "",
      selectedMsg: ""
    });
  };

  getUsers = async () => {
    this.setState({ selectedMsg: "", userInfo: "" });
    try {
      let response = await fetch(url);
      let json = await response.json();
      this.setState({ userJson: json, users: [] });
      let tempUsers = [];
      this.state.userJson.forEach(user => tempUsers.push(user));
      this.setState({
        users: tempUsers,
        snackbarMsg: "Server data loaded",
        selectedMsg: `${tempUsers.length} users loaded`,
        gotData: true,
        anchorEl: null
      });
    } catch (error) {
      console.log(error);
      this.setState({
        selectedMsg: `Problem loading server data - ${error.message}`,
        gotData: false,
        anchorEl: null
      });
    }
  };

  snackbarClose = () => {
    this.setState({ gotData: false });
  };

  // non optional method called at startup
  onSuggestionsClearRequested = () => {};
  render() {
    // use the state variables locally
    const {
      value,
      filteredUsers,
      userInfo,
      selectedMsg,
      gotData,
      snackbarMsg
    } = this.state;
    // seed the autosuggest
    const inputProps = {
      placeholder: "Type the name of the user",
      value,
      onChange: this.onChange
    };
    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ marginTop: "20%" }}>
          <CardHeader
            title="Lab #7"
            color="primary"
            style={{ textAlign: "center" }}
          />
          <CardContent>
            <Autosuggest
              suggestions={filteredUsers}
              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
              onSuggestionsClearRequested={this.onSuggestionsClearRequested}
              onSuggestionSelected={this.onSuggestionSelected}
              getSuggestionValue={getSuggestionValue}
              renderSuggestion={renderFilteredList}
              inputProps={inputProps}
            />
            {userInfo.length > 0 && (
              <Typography color="default">{userInfo}</Typography>
            )}
            {selectedMsg.length > 0 && (
              <Typography color="error">{selectedMsg}</Typography>
            )}
          </CardContent>
        </Card>
        <Snackbar
          open={gotData}
          message={snackbarMsg}
          autoHideDuration={4000}
          onClose={this.snackbarClose}
        />
      </MuiThemeProvider>
    );
  }
}
export default MaterialUIExLab7Search;

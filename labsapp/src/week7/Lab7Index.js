import React from "react";
import ReactDOM from "react-dom";
import MaterialUILab7Main from "./week7/lab7Main";
ReactDOM.render(<MaterialUILab7Main />, document.getElementById("root"));

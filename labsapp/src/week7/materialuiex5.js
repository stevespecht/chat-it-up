import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Toolbar,
  AppBar,
  Menu,
  MenuItem,
  IconButton,
  Typography
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Reorder from "@material-ui/icons/Reorder";
import MaterialUIEx5bComponent from "./materialuiex5b";
const fruits = [
  "Apple",
  "Apricot",
  "Avocado",
  "Banana",
  "Bilberry",
  "Blackberry",
  "Blackcurrant",
  "Blueberry",
  "Boysenberry",
  "Blood Orange",
  "Cantaloupe",
  "Currant",
  "Cherry",
  "Cherimoya",
  "Cloudberry",
  "Coconut",
  "Cranberry",
  "Clementine",
  "Damson",
  "Date",
  "Dragonfruit",
  "Durian",
  "Elderberry",
  "Feijoa",
  "Fig",
  "Goji berry",
  "Gooseberry",
  "Grape",
  "Grapefruit",
  "Guava",
  "Honeydew",
  "Huckleberry",
  "Jabouticaba",
  "Jackfruit",
  "Jambul",
  "Jujube",
  "Juniper berry",
  "Kiwi fruit",
  "Kumquat",
  "Lemon",
  "Lime",
  "Loquat",
  "Lychee",
  "Nectarine",
  "Mango",
  "Marion berry",
  "Melon",
  "Miracle fruit",
  "Mulberry",
  "Mandarine",
  "Olive",
  "Orange",
  "Papaya",
  "Passionfruit",
  "Peach",
  "Pear",
  "Persimmon",
  "Physalis",
  "Plum",
  "Pineapple",
  "Pumpkin",
  "Pomegranate",
  "Pomelo",
  "Purple Mangosteen",
  "Quince",
  "Raspberry",
  "Raisin",
  "Rambutan",
  "Redcurrant",
  "Salal berry",
  "Satsuma",
  "Star fruit",
  "Strawberry",
  "Squash",
  "Salmonberry",
  "Tamarillo",
  "Tamarind",
  "Tomato",
  "Tangerine",
  "Ugli fruit",
  "Watermelon"
];
class MaterialUIEx5Component extends React.PureComponent {
  state = {
    anchorEl: null,
    showFruitCard: false
  };
  onMenuItemClicked = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  onClose = () => {
    this.setState({ anchorEl: null });
  };
  onHomeItemClicked = () => {
    this.setState({ showFruitCard: false, anchorEl: null });
  };
  onFruitItemClicked = () => {
    this.setState({ showFruitCard: true, anchorEl: null });
  };
  render() {
    const { anchorEl, showFruitCard } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <IconButton onClick={this.onMenuItemClicked} color="inherit">
              <Reorder />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.onClose}
            >
              <MenuItem onClick={this.onHomeItemClicked}>Home</MenuItem>
              <MenuItem onClick={this.onFruitItemClicked}>
                Fruit Component
              </MenuItem>
            </Menu>
            <Typography variant="h6" color="inherit">
              INFO3069 - MaterialUI
            </Typography>
          </Toolbar>
        </AppBar>
        {!showFruitCard && (
          <Typography
            variant="h5"
            color="inherit"
            style={{
              textAlign: "center",
              marginTop: "4vh"
            }}
          >
            Exercise #5 - Home
          </Typography>
        )}
        {showFruitCard && (
          <MaterialUIEx5bComponent
            fruits={fruits}
            style={{ marginTop: "10%" }}
          />
        )}
      </MuiThemeProvider>
    );
  }
}
export default MaterialUIEx5Component;

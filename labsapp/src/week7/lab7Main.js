import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Toolbar,
  AppBar,
  Menu,
  MenuItem,
  IconButton,
  Typography
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Reorder from "@material-ui/icons/Reorder";
import MaterialUIExLab7Search from "./lab7Search";
class MaterialUILab7Main extends React.PureComponent {
  state = {
    anchorEl: null,
    showUserCard: false
  };
  onMenuItemClicked = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  onClose = () => {
    this.setState({ anchorEl: null });
  };
  onHomeItemClicked = () => {
    this.setState({ showUserCard: false, anchorEl: null });
  };
  onUserItemClicked = () => {
    this.setState({ showUserCard: true, anchorEl: null });
  };
  render() {
    const { anchorEl, showUserCard } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <IconButton onClick={this.onMenuItemClicked} color="inherit">
              <Reorder />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.onClose}
            >
              <MenuItem onClick={this.onHomeItemClicked}>Home</MenuItem>
              <MenuItem onClick={this.onUserItemClicked}>Load Users</MenuItem>
            </Menu>
            <Typography variant="h6" color="inherit">
              INFO3069 - MaterialUI
            </Typography>
          </Toolbar>
        </AppBar>
        {!showUserCard && (
          <Typography
            variant="h5"
            color="inherit"
            style={{
              textAlign: "center",
              marginTop: "4vh"
            }}
          >
            Lab 7 - Home
          </Typography>
        )}
        {showUserCard && (
          <MaterialUIExLab7Search
            url="http://127.0.0.1:5000/users"
            style={{ marginTop: "10%" }}
          />
        )}
      </MuiThemeProvider>
    );
  }
}
export default MaterialUILab7Main;

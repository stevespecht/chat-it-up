import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import theme from "./theme";
import Autosuggest from "react-autosuggest";
import "../App.css";
let fruits = [];
// non optional method - not used
const getSuggestionValue = suggestion => suggestion;
// renders filtered results
const renderFilteredList = selection => (
  <Typography variant="h6" color="inherit">
    {selection}
  </Typography>
);
// filtering code
const getSuggestions = value => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  return inputLength === 0
    ? ""
    : fruits.filter(
        fruit => fruit.toLowerCase().slice(0, inputLength) === inputValue
      );
};
class MaterialUIEx2Component extends React.PureComponent {
  state = {
    filteredfruits: [], // what we're searching through
    value: "", // what the user enters
    funFacts: "" // what we'll display when a selection is made
  };

  componentDidMount() {
    fruits = this.props.fruits;
  }
  // this stores the filtered list into state
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      filteredfruits: getSuggestions(value)
    });
  };
  // user has made a choice
  onSuggestionSelected = (event, { suggestion }) => {
    this.setState({
      funFacts: `Some interesting stuff for ${suggestion} would go here`
    });
  };
  // user has typed something in the input
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
      funFacts: ""
    });
  };
  // non optional method called at startup
  onSuggestionsClearRequested = () => {};
  render() {
    // use the state variables locally
    const { value, filteredfruits, funFacts } = this.state;
    // seed the autosuggest
    const inputProps = {
      placeholder: "Type the name of a fruit",
      value,
      onChange: this.onChange
    };
    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ marginTop: "20%" }}>
          <CardHeader
            title="Exercise #5"
            color="primary"
            style={{ textAlign: "center" }}
          />
          <CardContent>
            <Autosuggest
              suggestions={filteredfruits}
              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
              onSuggestionsClearRequested={this.onSuggestionsClearRequested}
              onSuggestionSelected={this.onSuggestionSelected}
              getSuggestionValue={getSuggestionValue}
              renderSuggestion={renderFilteredList}
              inputProps={inputProps}
            />
            {funFacts.length > 0 && (
              <Typography color="default">{funFacts}</Typography>
            )}
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default MaterialUIEx2Component;

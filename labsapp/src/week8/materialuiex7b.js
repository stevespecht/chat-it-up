import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Button,
  Card,
  CardHeader,
  CardContent,
  Typography
} from "@material-ui/core";
import theme from "../week7/theme";
import "../App.css";
class MaterialUIEx7bComponent extends React.PureComponent {
  state = {
    gotData: false,
    msgFromParent: this.props.dataForChild
  };
  sendParentSomeData = () => {
    this.props.dataFromChild("child component sent this!");
  };
  render() {
    const { msgFromParent } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ marginTop: "10%" }}>
          <CardHeader
            title="Exercise #7b - Child Component"
            color="inherit"
            style={{ textAlign: "center" }}
          />
          <CardContent>
            <Typography
              color="inherit"
              style={{ fontSize: "large", fontWeight: "bold" }}
            >
              {msgFromParent}
            </Typography>
            <br />
            <Button
              color="primary"
              variant="contained"
              onClick={this.sendParentSomeData}
            >
              Return Data
            </Button>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default MaterialUIEx7bComponent;

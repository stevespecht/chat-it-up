import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Snackbar, Typography } from "@material-ui/core";
import theme from "../week7/theme";
import MaterialUIEx7bComponent from "./materialuiex7b";
import "../App.css";
class MaterialUIEx7aComponent extends React.PureComponent {
  state = {
    snackbarMsg: "",
    msgFromParent: "data from parent",
    gotData: false
  };
  snackbarClose = () => {
    this.setState({ gotData: false });
  };
  msgFromChild = msg => {
    this.setState({ snackbarMsg: msg, gotData: true });
  };
  render() {
    const { snackbarMsg, msgFromParent, gotData } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <Typography color="primary" style={{ fontSize: "large" }}>
          Exercise #7a - Parent Component
        </Typography>
        <MaterialUIEx7bComponent
          dataFromChild={this.msgFromChild}
          dataForChild={msgFromParent}
        />
        <Snackbar
          open={gotData}
          message={snackbarMsg}
          autoHideDuration={4000}
          onClose={this.snackbarClose}
        />
      </MuiThemeProvider>
    );
  }
}
export default MaterialUIEx7aComponent;

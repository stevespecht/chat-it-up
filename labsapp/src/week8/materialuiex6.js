import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  IconButton,
  Snackbar,
  TextField
} from "@material-ui/core";
import theme from "../week7/theme";
import "../App.css";
import AddCircle from "@material-ui/icons/AddCircle";
class MaterialUIEx6Component extends React.PureComponent {
  state = {
    showMsg: false,
    snackbarMsg: "",
    name: "",
    age: 0,
    email: ""
  };
  onAddClicked = async () => {
    let user = {
      name: this.state.name,
      age: this.state.age,
      email: this.state.email
    };
    let userStr = JSON.stringify(user);
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    try {
      let response = await fetch("http://localhost:5000/users", {
        method: "POST",
        headers: myHeaders,
        body: userStr
      });
      let json = await response.json();
      this.setState({
        showMsg: true,
        snackbarMsg: json.msg,
        name: "",
        age: 0,
        email: ""
      });
    } catch (error) {
      this.setState({
        snackbarMsg: error.message,
        showMsg: true
      });
    }
  };
  snackbarClose = () => {
    this.setState({ showMsg: false });
  };
  handleNameInput = e => {
    this.setState({ name: e.target.value });
  };
  handleAgeInput = e => {
    let age = parseInt(e.target.value);
    age > 0 ? this.setState({ age: age }) : this.setState({ age: 0 });
  };
  handleEmailInput = e => {
    this.setState({ email: e.target.value });
  };
  render() {
    const { showMsg, snackbarMsg, name, age, email } = this.state;
    const emptyorundefined =
      name === undefined ||
      name === "" ||
      age === undefined ||
      age === 0 ||
      email === undefined ||
      email === "";
    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ marginTop: "10%" }}>
          <CardHeader
            title="Add A User"
            color="inherit"
            style={{ textAlign: "center" }}
          />
          <CardContent>
            <TextField
              onChange={this.handleNameInput}
              helperText="Enter user's name here"
              value={name}
            />
            <br />
            <TextField
              onChange={this.handleAgeInput}
              helperText="Enter user's age here"
              value={age}
            />
            <br />
            <TextField
              onChange={this.handleEmailInput}
              value={email}
              helperText="Enter user's email here"
            />
            <br />
            <IconButton
              color="secondary"
              style={{ marginTop: 50, float: "right" }}
              onClick={this.onAddClicked}
              disabled={emptyorundefined}
            >
              <AddCircle fontSize="large" />
            </IconButton>
            <Snackbar
              open={showMsg}
              message={snackbarMsg}
              autoHideDuration={4000}
              onClose={this.snackbarClose}
            />
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default MaterialUIEx6Component;

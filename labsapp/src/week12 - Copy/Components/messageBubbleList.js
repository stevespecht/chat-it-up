import React from "react";
import { List } from "@material-ui/core";
import MessageBubble from "./messageBubble";
const MessageBubbleList = props => {
  let messages = props.messages.map((message, messageIndex) => {
    let myMessage = false;

    if (props.myName === message.from) {
      myMessage = true;
    }

    return (
      <MessageBubble
        key={messageIndex}
        message={message}
        myMessage={myMessage}
      />
    );
  });
  return <List>{messages}</List>;
};
export default MessageBubbleList;

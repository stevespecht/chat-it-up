import React from "react";
import { Dialog, DialogTitle, DialogContent, Button } from "@material-ui/core";
import Chat from "@material-ui/icons/Chat";
import "../../App.css";
const DialogComponent = props => {
  let allInfo = props.getUserInfo().map((info, infoIndex) => {
    return (
      <div
        key={infoIndex}
        style={{ fontFamily: "Arial", margin: "3px", width: "200px" }}
      >
        <Chat
          style={{ color: info.colour, height: 20, width: 20, marginRight: 10 }}
        />
        {info.userName} is in room {info.room}
      </div>
    );
  });

  return (
    <div>
      <Dialog open={props.open} style={{ margin: 20 }}>
        <DialogTitle style={{ textAlign: "center" }}>Who's On?</DialogTitle>
        <DialogContent style={{ marginTop: "-10px" }}>
          {allInfo}
          <div style={{ textAlign: "center" }}>
            <Button
              variant="contained"
              color="primary"
              onClick={() => props.closeDialog()}
              style={{ textAlign: "center", marginTop: "10px" }}
            >
              Close
            </Button>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
};
export default DialogComponent;

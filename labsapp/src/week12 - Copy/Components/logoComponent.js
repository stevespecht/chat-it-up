//Name: Steven Specht, John Herring
//File: LogoComponent.js
//Date: 29/03/2019
//Purpose: log fof the app

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Typography, Card, CardHeader } from "@material-ui/core";
import theme from "./theme";
import "../App.css";

class LogoComponent extends React.PureComponent {
  state = {
    height: "",
    width: "",
    showText: true
  };

  componentDidMount() {
    this.setState({
      height: this.props.height,
      width: this.props.width
    });
  }

  render() {
    const { height, width } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <Typography
          variant="h5"
          color="inherit"
          style={{
            textAlign: "center",
            marginTop: "4vh"
          }}
        >
          <center>
            <img
              src={"signInLogoBlue.png"}
              alt="flaglogo"
              height={height}
              width={width}
            />
          </center>
        </Typography>
        <Typography
          style={{ textAlign: "center", fontSize: 30 }}
          color="secondary"
        >
          Sign In
        </Typography>
      </MuiThemeProvider>
    );
  }
}
export default LogoComponent;

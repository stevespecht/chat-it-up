import React from "react";
import io from "socket.io-client";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Button,
  TextField,
  Card,
  CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
  Typography,
  FormControl,
  AppBar,
  Toolbar
} from "@material-ui/core";
import theme from "./theme";
import "../../App.css";
import TopBar from "./topbar";
import MessageBubbleList from "./messageBubbleList";
import DialogChild from "./dialogchild";

class ChatClient extends React.PureComponent {
  state = {
    socket: null,
    chatName: "",
    roomName: "",
    previousRoomName: "",
    chatNameHelper: "",
    chatNameError: false,
    existingRooms: [],
    showSignUp: true,
    messages: [],
    msg: "",
    isTyping: false,
    disableSend: true,
    whosTyping: "",
    openDialog: false,
    userInfo: []
  };

  componentDidMount = () => {
    const socket = io.connect("localhost:5000", { forceNew: true });

    this.setState({ socket: socket }, () => {
      this.getExistingRooms();
    });
    socket.on("allRooms", this.setExistingRooms);
    socket.on("welcome", this.onWelcome);
    socket.on("nameexists", this.nameExists);
    socket.on("someonejoined", this.addMessage);
    socket.on("someoneleft", this.addMessage);
    socket.on("someoneistyping", this.typing);
    socket.on("nottyping", this.nottyping);
    socket.on("newmessage", this.addMessage);
    socket.on("setuserinfo", this.setUserInfo);
  };

  getExistingRooms = () => {
    this.state.socket.emit("getRooms");
    this.state.socket.emit("getUsers");
  };

  setExistingRooms = updatedRooms => {
    this.setState({ existingRooms: updatedRooms });
  };

  handleChatName = e => {
    this.setState({ chatName: e.target.value });
  };

  handleRoomName = e => {
    this.setState({ roomName: e.target.value }, function() {
      if (this.state.roomName === undefined || this.state.roomName === "") {
        this.setState({ roomName: this.state.existingRooms[0] });
      }
    });
  };

  handleExistingRooms = event => {
    this.setState({ roomName: event.target.value });
  };

  // handler for send message button
  handleJoinClick = () => {
    this.state.socket.emit("join", {
      chatName: this.state.chatName,
      roomName: this.state.roomName
    });
  };

  onWelcome = dataFromServer => {
    this.setState({ showSignUp: false });
    this.addMessage(dataFromServer);
  };

  nameExists = () => {
    this.setState({ chatNameError: true, chatNameHelper: "Name Exists" });
  };

  addMessage = dataFromServer => {
    this.setState(previousState => {
      let messages = [...previousState.messages];
      messages.push(dataFromServer);
      return { messages };
    });
  };

  typing = dataFromServer => {
    this.setState({ whosTyping: dataFromServer });
  };

  nottyping = () => {
    this.setState({ whosTyping: "" });
  };

  onMessageChange = e => {
    this.setState({ msg: e.target.value });

    if (e.target.value === "") {
      this.setState({ disableSend: true, isTyping: false });
      this.state.socket.emit("nottyping", {
        chatName: this.state.chatName,
        roomName: this.state.roomName
      });
    } else {
      this.setState({ disableSend: false });
    }

    if (this.state.isTyping === false) {
      this.state.socket.emit("typing", {
        chatName: this.state.chatName,
        roomName: this.state.roomName
      });
      this.setState({ isTyping: true });
    }
  };

  // handler for send message button
  handleSendMessage = e => {
    e.preventDefault();
    this.sendMessage();
  };

  keyPress = e => {
    if (e.keyCode === 13) {
      if (this.state.msg !== "") {
        this.sendMessage();
      }
    }
  };

  sendMessage = () => {
    this.state.socket.emit(
      "message",
      {
        chatName: this.state.chatName,
        roomName: this.state.roomName,
        text: this.state.msg
      },
      err => {}
    );
    this.state.socket.emit("nottyping", {
      chatName: this.state.chatName,
      roomName: this.state.roomName
    });
    this.setState({ msg: "", isTyping: false, disableSend: true });
  };

  handleOpenDialog = () => {
    this.state.socket.emit("getUsers");
    this.setState({ openDialog: true });
  };

  handleCloseDialog = () => this.setState({ openDialog: false });

  setUserInfo = dataFromServer => {
    this.setState({ userInfo: dataFromServer });
  };

  getUserInfo = () => {
    return this.state.userInfo;
  };

  render() {
    const {
      chatName,
      roomName,
      chatNameHelper,
      chatNameError,
      existingRooms,
      showSignUp,
      messages,
      msg,
      disableSend,
      whosTyping,
      openDialog
    } = this.state;

    const fieldsValid =
      chatName === undefined ||
      chatName === "" ||
      roomName === undefined ||
      roomName === "";

    let generateExistingRooms = existingRooms.map((room, roomIndex) => {
      return (
        <FormControlLabel
          key={roomIndex}
          value={room}
          control={<Radio />}
          label={room}
        />
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        <TopBar viewDialog={this.handleOpenDialog} />

        {showSignUp && (
          <Card style={{ marginTop: "5%" }}>
            <CardContent>
              <TextField
                placeholder="Chat Name"
                onChange={this.handleChatName}
                helperText={chatNameHelper}
                error={chatNameError}
              />
            </CardContent>
          </Card>
        )}
        {showSignUp && (
          <Card style={{ marginTop: "5%" }}>
            <CardContent>
              <Typography
                style={{
                  textAlign: "center",
                  fontWeight: 400,
                  fontSize: 20,
                  marginBottom: 20
                }}
              >
                Join Existing or Enter Room Name
              </Typography>
              <FormControl>
                <RadioGroup
                  value={this.state.roomName}
                  onChange={this.handleExistingRooms}
                  disabled={true}
                >
                  {generateExistingRooms}
                </RadioGroup>
                <TextField
                  placeholder="Room Name"
                  onChange={this.handleRoomName}
                />
              </FormControl>
            </CardContent>
          </Card>
        )}
        {showSignUp && (
          <Button
            variant="contained"
            color="secondary"
            style={{ marginTop: "5%" }}
            disabled={fieldsValid}
            onClick={this.handleJoinClick}
          >
            JOIN
          </Button>
        )}

        <DialogChild
          open={openDialog}
          getUserInfo={this.getUserInfo}
          closeDialog={this.handleCloseDialog}
        />
        {!showSignUp && (
          <div className="usersList">
            <MessageBubbleList messages={messages} myName={chatName} />
          </div>
        )}

        {!showSignUp && (
          <AppBar
            position="fixed"
            color={"default"}
            style={{ top: "auto", bottom: 0 }}
          >
            <Toolbar>
              <TextField
                onChange={this.onMessageChange}
                placeholder="type something here"
                autoFocus={true}
                required
                value={msg}
                onKeyDown={this.keyPress}
                style={{ width: "80%" }}
                helperText={whosTyping}
              />
              <Button
                variant="contained"
                color="secondary"
                style={{ marginLeft: "5%" }}
                onClick={this.handleSendMessage}
                disabled={disableSend}
              >
                SEND
              </Button>
            </Toolbar>
          </AppBar>
        )}
      </MuiThemeProvider>
    );
  }
}
export default ChatClient;

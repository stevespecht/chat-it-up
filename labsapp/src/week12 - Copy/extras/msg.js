import React from "react";
import "../App.css";
const Msg = props => {
  let msg = props.msg;
  return (
    <div className="scenario-message" style={{ backgroundColor: msg.colour }}>
      {msg.from} - {msg.text}
    </div>
  );
};
export default Msg;

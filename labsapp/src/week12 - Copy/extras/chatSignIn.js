import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Button,
  TextField,
  Card,
  CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
  Typography,
  FormControl
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import LogoComponent from "./logoComponent";

class ChatSignIn extends React.PureComponent {
  state = {
    chatName: "",
    roomName: "",
    previousRoomName: "",
    chatNameHelper: "",
    chatNameError: false,
    existingRooms: []
  };

  componentDidMount = () => {
    console.log("sign " + this.props.rooms);
    this.setState({ existingRooms: this.props.rooms }, function() {
      this.setState({ roomName: this.state.existingRooms[0] });
      console.log("sign " + this.state.existingRooms);
    });
  };

  handleChatName = e => {
    this.setState({ chatName: e.target.value });
  };

  handleRoomName = e => {
    this.setState({ roomName: e.target.value }, function() {
      if (this.state.roomName === undefined || this.state.roomName === "") {
        this.setState({ roomName: this.state.existingRooms[0] });
      }
    });
  };

  handleExistingRooms = event => {
    this.setState({ roomName: event.target.value });
  };

  // handler for send message button
  handleJoinClick = () => {
    //join here
    console.log(this.state.roomName);
  };

  render() {
    const {
      chatName,
      roomName,
      chatNameHelper,
      chatNameError,
      existingRooms
    } = this.state;

    const fieldsValid =
      chatName === undefined ||
      chatName === "" ||
      roomName === undefined ||
      roomName === "";

    let generateExistingRooms = existingRooms.map((room, roomIndex) => {
      return (
        <FormControlLabel
          key={roomIndex}
          value={room}
          control={<Radio />}
          label={room}
        />
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        <LogoComponent height="75" width="75" />
        <Card style={{ marginTop: "5%" }}>
          <CardContent>
            <TextField
              placeholder="Chat Name"
              onChange={this.handleChatName}
              helperText={chatNameHelper}
              error={chatNameError}
            />
          </CardContent>
        </Card>

        <Card style={{ marginTop: "5%" }}>
          <CardContent>
            <Typography
              style={{
                textAlign: "center",
                fontWeight: 400,
                fontSize: 20,
                marginBottom: 20
              }}
            >
              Join Existing or Enter Room Name
            </Typography>
            <FormControl>
              <RadioGroup
                value={this.state.roomName}
                onChange={this.handleExistingRooms}
                disabled={true}
              >
                {generateExistingRooms}
              </RadioGroup>
              <TextField
                placeholder="Room Name"
                onChange={this.handleRoomName}
              />
            </FormControl>
          </CardContent>
        </Card>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: "5%" }}
          disabled={fieldsValid}
          onClick={this.handleJoinClick}
        >
          JOIN
        </Button>
      </MuiThemeProvider>
    );
  }
}
export default ChatSignIn;

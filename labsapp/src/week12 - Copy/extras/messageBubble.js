import React from "react";
import ReactDOM from "react-dom";
import { ListItem } from "@material-ui/core";
import Bubble from "./bubble";
import Triangle from "./triangle";
class UserBubble extends React.Component {
  componentDidMount = () => {
    let userDOM = ReactDOM.findDOMNode(this);
    userDOM.scrollIntoView({ block: "end", behavior: "smooth" });
    userDOM.blur();
  };

  render() {
    let index = this.props.index;
    let color1 = "#1B5E20";
    let color2 = "#8B329F";
    let currentColour = color1;
    let currentAlign = "left";
    let leftWidth = "150%";
    let rightWidth = "150%";
    let currentWidth = leftWidth;
    let leftPosition = "20px";
    let rightPosition = "150px";
    let currentPosition = leftPosition;

    if (index % 3 === 0) {
      currentColour = color2;
      currentAlign = "right";
      currentWidth = rightWidth;
      currentPosition = rightPosition;
    } else {
      currentColour = color1;
      currentAlign = "left";
      currentWidth = leftWidth;
      currentPosition = leftPosition;
    }

    return (
      <div style={{ float: currentAlign }}>
        <ListItem
          ref="user"
          style={{
            textAlign: "left",
            marginBottom: "5px",
            width: currentWidth
          }}
        >
          <Bubble user={this.props.user} color={currentColour} />
          <Triangle color={currentColour} position={currentPosition} />
        </ListItem>
        &nbsp;
      </div>
    );
  }
}
export default UserBubble;

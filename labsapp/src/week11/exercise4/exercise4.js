import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Dialog, DialogTitle, DialogContent } from "@material-ui/core";
import theme from "./theme";
import "../../App.css";
import TopBar from "./topbar";
class Exercise4 extends React.PureComponent {
  state = {
    open: false
  };
  handleOpenDialog = () => this.setState({ open: true });
  handleCloseDialog = () => this.setState({ open: false });
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div>
          <TopBar viewDialog={this.handleOpenDialog} />
          <Dialog
            open={this.state.open}
            onClose={this.handleCloseDialog}
            style={{ margin: 20 }}
          >
            <DialogTitle style={{ textAlign: "center" }}>
              Some Dialog Info
            </DialogTitle>
            <DialogContent>Some interesting stuff would go here</DialogContent>
          </Dialog>
        </div>
      </MuiThemeProvider>
    );
  }
}
export default Exercise4;

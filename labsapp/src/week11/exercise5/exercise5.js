import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card, CardHeader, CardContent } from "@material-ui/core";
import theme from "./theme";
import UserBubbleList from "./userbubblelist";
import "../../App.css";
class Exercise3 extends React.PureComponent {
  state = {
    users: []
  };
  async componentDidMount() {
    let response = await fetch("http://localhost:5000/users");
    let payload = await response.json();
    this.setState({ users: payload });
  }
  render() {
    const { users } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ marginTop: "10%", marginLeft: "10%", width: "80%" }}>
          <CardHeader title="React List Exercise" />
          <CardContent style={{ textAlign: "center" }}>
            <div className="usersList">
              <UserBubbleList users={users} />
            </div>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default Exercise3;

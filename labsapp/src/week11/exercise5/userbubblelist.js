import React from "react";
import { List } from "@material-ui/core";
import UserBubble from "./userbubble";
const UserBubbleList = props => {
  let users = props.users.map(user => {
    return <UserBubble key={user._id} user={user} />;
  });
  return <List>{users}</List>;
};
export default UserBubbleList;

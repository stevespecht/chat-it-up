import React from "react";
import { List } from "@material-ui/core";
import UserBubble from "./lab10user";
const UserBubbleList = props => {
  let index = 1;

  let users = props.users.map(user => {
    index++;
    return <UserBubble key={user._id} user={user} index={index} />;
  });
  return <List>{users}</List>;
};
export default UserBubbleList;

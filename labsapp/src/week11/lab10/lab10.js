import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card, CardHeader, CardContent } from "@material-ui/core";
import theme from "./theme";
import UserBubbleList from "./lab10list";
import TopBar from "./topbar";
import DialogChild from "./dialogchild";
import "../../App.css";
class Lab10 extends React.PureComponent {
  state = {
    users: [],
    open: false,
    headerMsg: ""
  };
  async componentDidMount() {
    let response = await fetch("http://localhost:5000/users");
    let payload = await response.json();
    this.setState({ users: payload });
  }

  handleOpenDialog = () => this.setState({ open: true });

  handleCloseDialog = msgFromChild =>
    this.setState({ open: true, headerMsg: msgFromChild });

  render() {
    const { users, open, headerMsg } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <TopBar viewDialog={this.handleOpenDialog} />
        <DialogChild open={open} closeDialog={this.handleCloseDialog} />
        <Card style={{ marginTop: "10%", marginLeft: "10%", width: "80%" }}>
          {headerMsg !== "" && <CardHeader title={headerMsg} />}

          <CardContent style={{ textAlign: "center" }}>
            <div className="usersList">
              <UserBubbleList users={users} />
            </div>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default Lab10;

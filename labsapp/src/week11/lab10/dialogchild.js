import React from "react";
import { Dialog, DialogTitle, DialogContent, Button } from "@material-ui/core";
import "../../App.css";
const DialogComponent = props => {
  return (
    <div>
      <Dialog open={props.open} style={{ margin: 20 }}>
        <DialogTitle style={{ textAlign: "center" }}>LAB 10</DialogTitle>
        <DialogContent>
          <Button
            variant="contained"
            color="primary"
            onClick={() => props.closeDialog("Dialog was shown")}
          >
            Send data to parent
          </Button>
        </DialogContent>
      </Dialog>
    </div>
  );
};
export default DialogComponent;

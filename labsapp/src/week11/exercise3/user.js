import React from "react";
import ReactDOM from "react-dom";
import { ListItem } from "@material-ui/core";
class User extends React.PureComponent {
  componentDidMount = () => {
    let messageDOM = ReactDOM.findDOMNode(this);
    messageDOM.scrollIntoView({ block: "end", behavior: "smooth" });
    messageDOM.blur();
  };
  render() {
    let user = this.props.user;
    return (
      <ListItem ref="user" style={{ textAlign: "left" }}>
        <div style={{ fontSize: "smaller" }}>
          <span style={{ fontWeight: "bold" }}>Name: {user.name}</span>
          <br />
          <span>Age: {user.age}</span>
          <br />
          <span>Email: {user.email}</span>
        </div>
      </ListItem>
    );
  }
}
export default User;

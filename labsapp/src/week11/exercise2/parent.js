import React from "react";
import { Card, CardContent, TextField } from "@material-ui/core";
import Child from "./child";
class Parent extends React.PureComponent {
  state = {
    name: "",
    namesArray: ["Bill", "Sue", "Bob"]
  };
  handleNameChange = e => {
    let found = this.state.namesArray.find(name => name === e.target.value);
    found === undefined
      ? this.setState({ name: e.target.value })
      : this.setState({ name: "Name exists already" });
  };
  render() {
    return (
      <Card>
        <CardContent>
          <TextField
            placeholder="Enter a name"
            onChange={this.handleNameChange}
          />
          <Child name={this.state.name} />
        </CardContent>
      </Card>
    );
  }
}
export default Parent;

import React from "react";
class Child extends React.PureComponent {
  state = {
    name: "",
    emptyOrExists: true
  };
  static getDerivedStateFromProps(props, prevState) {
    // not using previous state
    return props.name.indexOf("exists") > 0 || props.name === ""
      ? { emptyOrExists: true, name: "name exists or is empty" }
      : { emptyOrExists: false, name: props.name };
  }
  render() {
    return (
      <div>
        {this.state.emptyOrExists && (
          <h3 style={{ color: "red" }}>{this.state.name}</h3>
        )}
        {!this.state.emptyOrExists && (
          <h3 style={{ color: "green" }}>{this.state.name}</h3>
        )}
      </div>
    );
  }
}
export default Child;

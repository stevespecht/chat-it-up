import React from "react";
const Child = props => {
  return (
    <div>
      <h1>{props.parentdata}</h1>
      <button onClick={() => props.childdata("some cool data from the kid")}>
        click
      </button>
    </div>
  );
};
export default Child;

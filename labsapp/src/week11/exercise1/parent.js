import React from "react";
import Child from "./child";
class Parent extends React.PureComponent {
  state = {
    childData: ""
  };
  handleChildSentData(somevalue) {
    this.setState({ childData: somevalue });
  }
  render() {
    return (
      <div>
        <Child
          parentdata="Parent data"
          childdata={someDataFromChild =>
            this.handleChildSentData(someDataFromChild)
          }
        />
        {this.state.childData !== "" && (
          <div>
            <h3>child component sent back</h3>
            {this.state.childData}
          </div>
        )}
      </div>
    );
  }
}
export default Parent;

import React from "react";
import ReactDOM from "react-dom";
import { ListItem } from "@material-ui/core";
import Bubble from "./bubble";
import Triangle from "./triangle";
class UserBubble extends React.Component {
  componentDidMount = () => {
    let userDOM = ReactDOM.findDOMNode(this);
    userDOM.scrollIntoView({ block: "end", behavior: "smooth" });
    userDOM.blur();
  };

  render() {
    let currentAlign;
    let currentWidth;
    let currentPosition;
    let currentRightMargin;

    if (this.props.myMessage) {
      currentAlign = "right";
      currentWidth = "300px";
      currentPosition = "170px";
      currentRightMargin = "-100px";
    } else {
      currentAlign = "left";
      currentWidth = "300px";
      currentPosition = "20px";
      currentRightMargin = "0px";
    }

    return (
      <div style={{ float: currentAlign }}>
        <ListItem
          ref="user"
          style={{
            textAlign: "left",
            marginBottom: "5px",
            marginRight: currentRightMargin,
            width: currentWidth
          }}
        >
          <Bubble message={this.props.message} />
          <Triangle
            colour={this.props.message.colour}
            position={currentPosition}
          />
        </ListItem>
        &nbsp;
      </div>
    );
  }
}
export default UserBubble;

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Button,
  TextField,
  Card,
  CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
  Typography,
  FormControl
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import LogoComponent from "./logoComponent";
const Child = props => {
  return (
    <MuiThemeProvider theme={theme}>
      <LogoComponent height="75" width="75" />
      <Card style={{ marginTop: "5%" }}>
        <CardContent>
          <TextField
            placeholder="Chat Name"
            onChange={props.handleChatName}
            helperText={props.chatNameHelper}
            error={props.chatNameError}
          />
        </CardContent>
      </Card>

      <Card style={{ marginTop: "5%" }}>
        <CardContent>
          <Typography
            style={{
              textAlign: "center",
              fontWeight: 400,
              fontSize: 20,
              marginBottom: 20
            }}
          >
            Join Existing or Enter Room Name
          </Typography>
          <FormControl>
            <RadioGroup
              // value={this.state.roomName}
              onChange={props.handleExistingRooms}
              disabled={true}
            >
              {props.generateExistingRooms}
            </RadioGroup>
            <TextField
              placeholder="Room Name"
              onChange={props.handleRoomName}
            />
          </FormControl>
        </CardContent>
      </Card>
      <Button
        variant="contained"
        color="secondary"
        style={{ marginTop: "5%" }}
        disabled={props.fieldsValid}
        onClick={props.handleJoinClick}
      >
        JOIN
      </Button>
    </MuiThemeProvider>
  );
};
export default Child;

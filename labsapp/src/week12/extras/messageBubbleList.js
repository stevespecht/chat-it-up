import React from "react";
import { List } from "@material-ui/core";
import MessageBubble from "./messageBubble";
const MessageBubbleList = props => {
  let index = 1;

  let users = props.users.map(user => {
    index++;
    return <MessageBubble key={user._id} user={user} index={index} />;
  });
  return <List>{users}</List>;
};
export default MessageBubbleList;

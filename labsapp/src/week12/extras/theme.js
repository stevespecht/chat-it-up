import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "#fafafa" },
    primary: {
      light: "rgba(242, 192, 108, 1)",
      main: "rgba(245, 166, 35, 1)",
      dark: "rgba(245, 166, 35, 1)",
      contrastText: "#fff"
    },
    secondary: {
      light: "rgba(126, 186, 255, 1)",
      main: "rgba(74, 144, 226, 1)",
      dark: "rgba(27, 94, 173, 1)",
      contrastText: "#fff"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});

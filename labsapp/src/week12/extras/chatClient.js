import React from "react";
import io from "socket.io-client";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Button,
  TextField,
  Card,
  CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
  Typography,
  FormControl
} from "@material-ui/core";
import theme from "./theme";
import Msg from "./msg";
import "../App.css";
import TopBar from "./topbar";
import MessageBubbleList from "./messageBubbleList";

class ChatClient extends React.PureComponent {
  state = {
    socket: null,
    chatName: "",
    roomName: "",
    previousRoomName: "",
    chatNameHelper: "",
    chatNameError: false,
    existingRooms: [],
    showSignUp: true,
    messages: [],
    msg: "",
    isTyping: false
  };

  componentDidMount = () => {
    const socket = io.connect("localhost:5000", { forceNew: true });

    this.setState({ socket: socket }, () => {
      this.getExistingRooms();
    });
    socket.on("allRooms", this.setExistingRooms);

    socket.on("welcome", this.onWelcome);

    socket.on("nameexists", this.nameExists);
    socket.on("someonejoined", this.addMessage);
    socket.on("someoneleft", this.addMessage);
    socket.on("someoneistyping", this.addMessage);
    socket.on("newmessage", this.addMessage);
  };

  getExistingRooms = () => {
    this.state.socket.emit("getRooms");
  };

  setExistingRooms = updatedRooms => {
    this.setState({ existingRooms: updatedRooms });
  };

  handleChatName = e => {
    this.setState({ chatName: e.target.value });
  };

  handleRoomName = e => {
    this.setState({ roomName: e.target.value }, function() {
      if (this.state.roomName === undefined || this.state.roomName === "") {
        this.setState({ roomName: this.state.existingRooms[0] });
      }
    });
  };

  handleExistingRooms = event => {
    this.setState({ roomName: event.target.value });
  };

  // handler for send message button
  handleJoinClick = () => {
    this.setState({ showSignUp: false });
    this.state.socket.emit("join", {
      chatName: this.state.chatName,
      roomName: this.state.roomName
    });
  };

  onWelcome = dataFromServer => {
    console.log(dataFromServer);
    this.addMessage(dataFromServer);
  };

  nameExists = () => {
    this.setState({ chatNameError: true, chatNameHelper: "Name Exists" });
  };

  addMessage = dataFromServer => {
    this.setState(previousState => {
      let messages = [...previousState.messages];
      messages.push(dataFromServer);
      return { messages };
    });
  };

  onMessageChange = e => {
    this.setState({ msg: e.target.value });
    if (this.state.isTyping === false) {
      this.state.socket.emit("typing", {
        chatName: this.state.chatName,
        roomName: this.state.roomName
      });
      this.setState({ isTyping: true });
    }
  };

  // handler for send message button
  handleSendMessage = e => {
    e.preventDefault();
    this.state.socket.emit(
      "message",
      {
        chatName: this.state.chatName,
        roomName: this.state.roomName,
        text: this.state.msg
      },
      err => {}
    );
    this.setState({ msg: "", isTyping: false });
  };

  render() {
    const {
      chatName,
      roomName,
      chatNameHelper,
      chatNameError,
      existingRooms,
      showSignUp,
      messages
    } = this.state;

    const fieldsValid =
      chatName === undefined ||
      chatName === "" ||
      roomName === undefined ||
      roomName === "";

    let generateExistingRooms = existingRooms.map((room, roomIndex) => {
      return (
        <FormControlLabel
          key={roomIndex}
          value={room}
          control={<Radio />}
          label={room}
        />
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        <TopBar viewDialog={this.handleOpenDialog} />

        {showSignUp && (
          <Card style={{ marginTop: "5%" }}>
            <CardContent>
              <TextField
                placeholder="Chat Name"
                onChange={this.handleChatName}
                helperText={chatNameHelper}
                error={chatNameError}
              />
            </CardContent>
          </Card>
        )}
        {showSignUp && (
          <Card style={{ marginTop: "5%" }}>
            <CardContent>
              <Typography
                style={{
                  textAlign: "center",
                  fontWeight: 400,
                  fontSize: 20,
                  marginBottom: 20
                }}
              >
                Join Existing or Enter Room Name
              </Typography>
              <FormControl>
                <RadioGroup
                  value={this.state.roomName}
                  onChange={this.handleExistingRooms}
                  disabled={true}
                >
                  {generateExistingRooms}
                </RadioGroup>
                <TextField
                  placeholder="Room Name"
                  onChange={this.handleRoomName}
                />
              </FormControl>
            </CardContent>
          </Card>
        )}
        {showSignUp && (
          <Button
            variant="contained"
            color="secondary"
            style={{ marginTop: "5%" }}
            disabled={fieldsValid}
            onClick={this.handleJoinClick}
          >
            JOIN
          </Button>
        )}

        <Card>
          <CardContent>
            <MessageBubbleList users={users} />
          </CardContent>
        </Card>
        {/* <div className="scenario-container">
          {messages.map((message, index) => (
            <Msg msg={message} key={index} />
          ))}
        </div> */}
      </MuiThemeProvider>
    );
  }
}
export default ChatClient;
